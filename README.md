![TfE_HeadLogo](./Media/ToastsFromExcel_HeadLogo.png)

---

<i><h3 style="color:grey">NOTE: This branch is for developing new functionalities for current project.</h3></i>

<u>**Important note: This project actively uses PowerShell's [Baron's Toasts](https://gitlab.com/BaronProsimir/barons_picnic/-/tree/Developer/BaronsToasts/0.0.1) module for work. Make sure you have installed it before using this 😉.**</u>

This VBA project was made to manipulate with [Toast notifications](https://docs.microsoft.com/en-us/windows/apps/design/shell/tiles-and-notifications/toast-ux-guidance) from [Microsoft's Excel](https://www.microsoft.com/en-us/microsoft-365/excel).

<br/>

*Example of using:*
``` VB
  ' Declare new class variable:
  Dim XMLsource As SourceXMLFile

  ' Build Toast notification's body and send the Toast:
  Public Sub SendToast()
    Set XMLsource = New SourceXMLFile
    With xmlsource
      .TextHighlighted = "Text: Highlighted"
      .TextLine1 = "Text: Line 1"
      .TextLine2 = "Text: Line 2"
      .TextAttribution = "Text: Attribution"
      .ProgressBar "Status", "Title", 0.5, "ValueStringOverride"
      .Sound = Default
      .CreateNewNotification
      .SendNotification NotificationTag:="Test", NotificationGroup:="TestGroup"
    End With
  End Sub

  ' Send Toast notification:
  Call SendToast

  ' Kill the object:
  Set XMLsource = Nothing
```
The result:  
![CodeResult](./Media/CodeExampleToast.png "Result of previous code example.")

<br />

`Created by: © BaronProsimir - 2021`  
`Licence:` [`GNU-3.0`](https://www.gnu.org/licenses/gpl-3.0.en.html)  
`Version: 0.0.1`  
