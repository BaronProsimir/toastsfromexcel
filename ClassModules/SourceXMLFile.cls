VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
END
Attribute VB_Name = "SourceXMLFile"
Attribute VB_GlobalNameSpace = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Attribute VB_Creatable = False

Option Explicit ' All variables have to be declared before initialization!
' +------------------------------------------------------------------------+  '
' |--------------- Created By: BaronProsimir 2021 ------------------| '
' |-Description: Handling Source XML file for toast notification.-| '
' + -----------------+-----------------------------------------------------+  '
' | Version: 0.0.2 |
' +------------------+

' #REGION Variables

Public Enum DefaultWindowsSounds
  Attribute DefaultWindowsSounds.VB_Description = "Defauls sounds in Windows 10."
  Default
  IM
  Mail
  Reminder
  SMS
  Alarm1
  Alarm2
  Alarm3
  Alarm4
  Alarm5
  Alarm6
  Alarm7
  Alarm8
  Alarm9
  Alarm10
  Call1
  Call2
  Call3
  Call4
  Call5
  Call6
  Call7
  Call8
  Call9
  Call10
End Enum

Public Enum ImagePlacement
  AppLogoOverride
  Hero
  Inline
End Enum

Public Enum ImageCrop
  def
  None
  InCircle
End Enum

' Defaults:
Private Const dFN As String = "ToastXml_FromExcel" ' Default XML file name.
Private Const dHT As String = "Excel information" ' Default notification's highlighted text
Private Const dNG As String = "DefaultExcelNotificationGroup"
Private Const dNT As String = "DefaultExcelNotificationTag"

Private Const dPSMP As String = "\WindowsPowerShell\Modules\" ' Default PS-Modules path.
Private Const CurrentModuleVersion As String = "0.0.1"
Private Const ModuleName As String = "ToastsFromExcel"

Private fP As String ' XML file path.
Private fN As String ' XML file name.
Private fso As Scripting.FileSystemObject ' File-system Object.

Private nG As String ' Notification Group.
Private nT As String ' Notification Tag.

' Notification's attributes:
Private hT As String ' HighlitedText
Private L1 As String ' Line1
Private L2 As String ' Line2
Private att As String ' Attribution text.
Private img As String ' Image.
Private SoundSelected As Boolean ' User-selected sound.
Private s As String ' Notification's sound.

' Progress bar attributes:
Private AddProgress As Boolean ' User add progress bar.
Private pb_title As String ' Progress bar title.
Private pb_value As Double ' Progress bar value.
Private pb_valueStringOverride As String ' Progress bar override string.
Private pb_status As String ' Progress bar status.

' Image attributes:
Private AddImage As Boolean ' User add image.
Private img_placement As ImagePlacement ' Image placement.
Private img_hCrop As ImageCrop ' Image hint crop.
Private img_src As String ' Image path.

' #ENDREGION Variables
'
' ---------------------------
'
' #REGION Constructor and Destructor

Private Sub class_initialize()
  fP = CreateObject("WScript.Shell").SpecialFolders("MyDocuments") & dPSMP & ModuleName & "\" & CurrentModuleVersion & "\"
  fN = dFN & ".xml"
  hT = dHT
  nT = dNT
  nG = dNG
  AddProgress = False
  AddImage = False
  Set fso = New Scripting.FileSystemObject
End Sub

Private Sub class_terminate()
  Set fso = Nothing
End Sub

' #ENDREGION Constructor and Destructor
'
' ---------------------------
'
' #REGION Properties

Public Property Get FileName() As String
  Attribute FileName.VB_Description = "Gets or sets XML source file's name."
  FileName = fN
End Property
Public Property Let FileName(NewName As String)
  Attribute FileName.VB_Description = "Gets or sets XML source file's name."
  ' Add XML extension on new name if missing. '
  If Right(NewName, 4) <> ".xml" Then NewName = NewName & ".xml"
  fN = NewName
End Property

Public Property Get FilePath() As String
  Attribute FilePath.VB_Description = "Gets or sets path, where XML source file will be saved."
  FilePath = fP
End Property
Public Property Let FilePath(NewPath As String)
  Attribute FilePath.VB_Description = "Gets or sets path, where XML source file will be saved."
  ' Add '\' symbol on the end of the path if missing. '
  If Right(NewPath, 1) <> "\" Then NewPath = NewPath & "\"
  fP = FilePath
End Property

Public Property Get IsFileExist() As Boolean
  Attribute IsFileExist.VB_Description = "Checks, if file FileName on FilePath exists."
  Attribute IsFileExist.VB_UserMemId = 0
  IsFileExist = fso.FileExists(FilePath & FileName)
End Property

Public Property Let TextHighlighted(Text As String)
  Attribute TextHighlighted.VB_Description = "Sets Notification's first line text (highlighted)."
  hT = Text
End Property

Public Property Let TextLine1(Text As String)
  Attribute TextLine1.VB_Description = "Sets Notification's first non-highlighted text."
  L1 = Text
End Property

Public Property Let TextLine2(Text As String)
  Attribute TextLine2.VB_Description = "Sets Notification's second non-highlighted text."
  L2 = Text
End Property

Public Property Let TextAttribution(Text As String)
  Attribute TextAttribution.VB_Description = "Sets Notification's attribution text."
  att = Text
End Property

Public Property Let Sound(NewSound As DefaultWindowsSounds)
  Attribute Sound.VB_Description = "Sets sound, which will be played when Notification is displayed."
  Select Case NewSound
    Case 0
      s = "ms-winsoundevent:Notification.Default"
    Case 1
      s = "ms-winsoundevent:Notification.IM"
    Case 2
      s = "ms-winsoundevent:Notification.Mail"
    Case 3
      s = "ms-winsoundevent:Notification.Reminder"
    Case 4
      s = "ms-winsoundevent:Notification.SMS"
    Case 5
      s = "ms-winsoundevent:Notification.Looping.Alarm"
    Case 6
      s = "ms-winsoundevent:Notification.Looping.Alarm2"
    Case 7
      s = "ms-winsoundevent:Notification.Looping.Alarm3"
    Case 8
      s = "ms-winsoundevent:Notification.Looping.Alarm4"
    Case 9
      s = "ms-winsoundevent:Notification.Looping.Alarm5"
    Case 10
      s = "ms-winsoundevent:Notification.Looping.Alarm6"
    Case 11
      s = "ms-winsoundevent:Notification.Looping.Alarm7"
    Case 12
      s = "ms-winsoundevent:Notification.Looping.Alarm8"
    Case 13
      s = "ms-winsoundevent:Notification.Looping.Alarm9"
    Case 14
      s = "ms-winsoundevent:Notification.Looping.Alarm10"
    Case 15
      s = "ms-winsoundevent:Notification.Looping.Call"
    Case 16
      s = "ms-winsoundevent:Notification.Looping.Call2"
    Case 17
      s = "ms-winsoundevent:Notification.Looping.Call3"
    Case 18
      s = "ms-winsoundevent:Notification.Looping.Call4"
    Case 19
      s = "ms-winsoundevent:Notification.Looping.Call5"
    Case 20
      s = "ms-winsoundevent:Notification.Looping.Call6"
    Case 21
      s = "ms-winsoundevent:Notification.Looping.Call7"
    Case 22
      s = "ms-winsoundevent:Notification.Looping.Call8"
    Case 23
      s = "ms-winsoundevent:Notification.Looping.Call9"
    Case 24
      s = "ms-winsoundevent:Notification.Looping.Call10"
  End Select
  SoundSelected = True
End Property

Public Property Get Group() As String
  Attribute Group.VB_Description = "Gets or sets Notification group."
  NotificationGroup = nG
End Property
Public Property Let Group(NewGroup As String)
  Attribute Group.VB_Description = "Gets or sets Notification group."
  nG = NewGroup
End Property

Public Property Get Tag() As String
  Attribute Tag.VB_Description = "Gets or sets Notification's tag."
  NotificationTag = nT
End Property
Public Property Let Tag(NewTag As String)
  Attribute Tag.VB_Description = "Gets or sets Notification's tag."
  nT = NewTag
End Property

' #ENDREGION Properties
'
' ---------------------------
'
' #REGION Interface
  
Public Sub CreateFile(Optional Path, Optional Name, Optional Verbose As Boolean = False)
  Attribute CreateFile.VB_Description = "Create XML source file."
  ' Handle missing optional parameter 'Path'. '
  Dim p As String: p = Switch(IsMissing(Path), fP, Not IsMissing(Path), Path)
  ' Add '\' symbol on the end of the path if missing. '
  If Right(p, 1) <> "\" Then p = p & "\"
  ' Handle missing optional parameter 'Name'. '
  Dim n As String: n = Switch(IsMissing(Name), fN, Not IsMissing(Name), Name)
  ' Add XML extension on the name if missing. '
  If Right(n, 4) <> ".xml" Then n = n & ".xml"
  ' Create XML file `n` on `p` path. '
  fso.CreateTextFile FileName:=p & n, overwrite:=True
  ' Write default Toast notification XML body into new created file. '
  fso.OpenTextFile(p & n, ForWriting).Write BuildNotificationBody
  ' Inform user about file creation if 'Verbose' parameter was set. '
  If Verbose Then Debug.Print "File " & n & " was created on following path: " & p
  
End Sub

Public Function ProgressBar(Status As String, Optional Title As String = "", Optional Value As Double = 0#, Optional ValueStringOverride As String = "") As Boolean
  Attribute ProgressBar.VB_Description = "Add progress bar to the Notification."
  pb_status = Status
  pb_title = Title
  pb_value = Value
  pb_valueStringOverride = ValueStringOverride
  AddProgress = True
End Function
  
Public Sub Image(Placement As ImagePlacement, ImagePath As String, Optional Crop As ImageCrop = ImageCrop.def)
  img_placement = Placement
  img_hCrop = Crop
  img_src = ImagePath
  AddImage = True
End Sub

Public Sub Send(Optional NotificationGroup As String, Optional NotificationTag As String)
  Attribute Send.VB_Description = "Send Notification to current logged user."
  Dim command As String
    ' Handle optional 'Tag' parameter. '
  NotificationTag = Switch(NotificationTag = "", nT, NotificationTag <> "", NotificationTag)
  ' Handle optional 'Group' parameter. '
  NotificationGroup = Switch(NotificationGroup = "", nG, NotificationGroup <> "", NotificationGroup)
  
  command = "powershell -WindowStyle ""hidden"" -command Send-BP-Toast -FileName " & fN & " -Tag " & NotificationTag & " -Group " & NotificationGroup
  ' Send Toast notification with "Send-BP-Toast" powershell module. '
  CreateObject("WScript.Shell").exec command
End Sub

' #ENDREGION Interface
'
' ---------------------------
'
' #REGION Implementation

Private Function BuildNotificationBody() As String
  ' ### DESTCRIPTION: Build final notification's XML source file. ### '
    
  BuildNotificationBody = "<toast>" & vbNewLine
  BuildNotificationBody = BuildNotificationBody & vbTab & "<visual>" & vbNewLine
  BuildNotificationBody = BuildNotificationBody & vbTab & vbTab & "<binding template = ""ToastGeneric"">" & vbNewLine
  If AddImage Then BuildNotificationBody = BuildNotificationBody & vbTab & vbTab & "<image " & _
    "placement=""" & Switch(img_placement = AppLogoOverride, "appLogoOverride", img_placement = Hero, "Hero", img_placement = Inline, "Inline") & _
    """ hint-crop=""" & Switch(img_hCrop = def, "Default", img_hCrop = InCircle, "Circle", img_hCrop = None, "None") & _
    """ src=""" & img_src & """ />" & vbNewLine
  BuildNotificationBody = BuildNotificationBody & vbTab & vbTab & vbTab & "<text hint-maxLines=""4"">" & hT & "</text>" & vbNewLine
  BuildNotificationBody = BuildNotificationBody & vbTab & vbTab & vbTab & "<text>" & L1 & "</text>" & vbNewLine
  BuildNotificationBody = BuildNotificationBody & vbTab & vbTab & vbTab & "<text>" & L2 & "</text>" & vbNewLine
  BuildNotificationBody = BuildNotificationBody & vbTab & vbTab & vbTab & "<text placement=""attribution"">" & att & "</text>" & vbNewLine
  If AddProgress Then BuildNotificationBody = BuildNotificationBody & vbTab & vbTab & vbTab & "<progress status = """ & pb_status & """ value = """ & Replace(pb_value, ",", ".") & """ title = """ & pb_title & """ valueStringOverride = """ & pb_valueStringOverride & """ />" & vbNewLine
  BuildNotificationBody = BuildNotificationBody & vbTab & vbTab & "</binding>" & vbNewLine
  BuildNotificationBody = BuildNotificationBody & vbTab & "</visual>" & vbNewLine
  If SoundSelected Then BuildNotificationBody = BuildNotificationBody & vbTab & "<audio src = """ & s & """ />" & vbNewLine
  BuildNotificationBody = BuildNotificationBody & "</toast>"
End Function

' #ENDREGION Implementation
'
' ---------------------------
' #REGION Debug

Private Sub Debugerer()
  ' Print current notification body. '
  Debug.Print BuildNotificationBody
End Sub

' #ENDREGION Debug

