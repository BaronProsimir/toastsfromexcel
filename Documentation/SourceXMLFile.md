# **SourceXMLFile Class** #
##### Namespace: [ToastXMLhandleProject](./ToastXMLhandleProject.md) #####

<br/>

### Create new Toast Notification's XML source object. ###

<br />

# **Example** #

Following examle shows how to create and send Toast Notification from Excel's VBA macro.

```VBS
Public Sub CreateAndSendNewNotification()
  Dim xmlFile As New SourceXMLFile
  With xmlfile
    .FileName = "UpdateXML.xml"
    .FilePath = "C:\files\XML\"
    .Sound = Alarm1
    .TextAttribution = "From FileUpdater"
    .TextHighlighted = "Warning! New update is ready."
    .TextLine1 = "New update will be installed soon."
    .TextLine2 = "Please, hold on..."
    .ProgressBar "Update", 0.0, "Update finished:" "0/100"
    .Group = "Updates"
    .Tag = "Update"
    .CreateFile
    .Send
  End With
  Set xmlFile = Nothing
End Sub
```
<br />

# **Constructors** #

| Constructor | Descritption|
|:---|---:|
|[SourceXMLFile](#sourcexmlfile-constructor "SourceXMLFile Constructor") | Create new instance of [SourceXMLFile](#sourcexmlfile-class "SourceXmlFile Class") class.|

<br />

# **Enums** #

| Enum | Descritption|
|:---|---:|
| [DefaultWindowsSounds](#defaultwindowssounds-enum "DefaultWindowsSounds Enum") | Defauls sound in Windows 10. |

<br />

# **Methods** #
  
| Method | Descritption|
|:---|---:|
| [CreateFile (*Path*, *Name*, *Verbose*)](#createfile-method "CreateFile Method") | Create XML source file. |
| [ProgressBar (Status, Value, *Title*, *ValueStringOverride*)](#progressbar-method "ProgressBar Method") | Add progress bar to the Notification. |
| [Send (*NotificationGroup*, *NotificationTag*)](#send-method "Send Method") | Send Notification to current logged user. |

<br />

# **Properties** #

| Property | Descritption|
|:---|---:|
| [FileName](#filename-property "FileName Property")| Gets or sets XML source file's name. |
| [FilePath](#filepath-property "FilePath Property") | Gets or sets path, where XML source file will be saved. |
| [Group](#group-property "Group Property") | Gets or sets Notification group. |
| [IsFileExist](#isfileexist-property) | Checks, if file [`FileName`](#filepath-property "FileName Property") on [`FilePath`](#filepath-property "FilePath Property") exists. |
| [Sound](#sound-property "Sound Property") | Sets sound, which will be played when Notification is displayed. |
| [Tag](#tag-property "Tag Property") | Gets or sets Notification's tag. |
| [TextAttribution](#textattribution-property "TextAttribution Property") | Sets Notification's attribution text. | 
|[TextHighlighted](#texthighlighted-property "TextHighlighted Property") | Sets Notification's first line text (highlighted). |
| [TextLine1](#textline1-property "TextLine1 Property") | Sets Notification's first non-highlighted text. |
| [TextLine2](#textline2-property "TextLine2 Property") | Sets Notification's second non-highlighted text. |

<br />

# **Details** #

<br />

# SourceXMLFile constructor #

```vbs
Dim xmlsource As New SourceXMLFile
```
or
```VBS
Dim xmlsource As SourceXMLFile
Set xmlsource = New sourceXMLFile
```
<br />

# DefaultWindowsSounds Enum #

| Constant | Value | Description |
|:---|:---:|---:|
| Default | 0 | ms-winsoundevent:Notification.Default |
| IM | 1 | ms-winsoundevent:Notification.IM |
| Mail | 2 | ms-winsoundevent:Notification.Mail |
| Reminder | 3 | ms-winsoundevent:Notification.Reminder |
| SMS | 4 | ms-winsoundevent:Notification.SMS |
| Alarm1 | 5 | ms-winsoundevent:Notification.Looping.Alarm |
| Alarm2 | 6 | ms-winsoundevent:Notification.Looping.Alarm2 |
| Alarm3 | 7 | ms-winsoundevent:Notification.Looping.Alarm3 |
| Alarm4 | 8 | ms-winsoundevent:Notification.Looping.Alarm4 |
| Alarm5 | 9 | ms-winsoundevent:Notification.Looping.Alarm5 |
| Alarm6 | 10 | ms-winsoundevent:Notification.Looping.Alarm6 |
| Alarm7 | 11 | ms-winsoundevent:Notification.Looping.Alarm7 |
| Alarm8 | 12 | ms-winsoundevent:Notification.Looping.Alarm8 |
| Alarm9 | 13 | ms-winsoundevent:Notification.Looping.Alarm9 |
| Alarm10 | 14 | ms-winsoundevent:Notification.Looping.Alarm10 |
| Call1 | 15 | ms-winsoundevent:Notification.Looping.Call |
| Call2 | 16 | ms-winsoundevent:Notification.Looping.Call2 |
| Call3 | 17 | ms-winsoundevent:Notification.Looping.Call3 |
| Call4 | 18 | ms-winsoundevent:Notification.Looping.Call4 |
| Call5 | 19 | ms-winsoundevent:Notification.Looping.Call5 |
| Call6 | 20 | ms-winsoundevent:Notification.Looping.Call6 |
| Call7 | 21 | ms-winsoundevent:Notification.Looping.Call7 |
| Call8 | 22 | ms-winsoundevent:Notification.Looping.Call8 |
| Call9 | 23 | ms-winsoundevent:Notification.Looping.Call9 |
| Call10 | 24 | ms-winsoundevent:Notification.Looping.Call10 |

<br />

# FileName Property #

Gets or sets XML source file's name.
```VBS
public Property Get FileName() As String
Public Property Let FileName(NewName As String)
```
## Property Value ##
[String](https://docs.microsoft.com/en-us/dotnet/api/system.string?view=net-5.0 "String reference.")

## Default Value ##
```VBS
"ToastXML_FromExcel.xml"
```

## Remarks ##
You can set name with or without ".xml" extension, property will add it automatically for you.
```VBS
xmlSource.Filename = "MySourceFile"
Debug.Print xmlSource.Filename ' MySourceFile.xml

xmlSource.Filename = "MySourceFile.xml"
Debug.Print xmlSource.FileName ' MysourceFile.xml
```


<br />

# FilePath Property #

Gets or sets path, where XML source file will be saved.
```VBS
Public Property Get FilePath() As String
Public Property Let FilePath(NewPath As String)
```
## Property Value ##
[String](https://docs.microsoft.com/en-us/dotnet/visual-basic/language-reference/data-types/string-data-type "String reference.")

## Default Value ##
```VBS
"C\Users\USERNAME\Documents\WindowsPowerShell\Modules\MODULE_NAME\CURRENT_MODULES_VERSION\"
```

## Remarks ##
You can set path with or without last "\" symbol, property will add it automatically for you.
```VBS
xmlSource.FilePath = "C:\MySources\XML"
Debug.Print xmlSource.FilePath ' C:\MySources\XML\

xmlSource.FilePath = "C:\MySources\XML\"
Debug.Print xmlSource.FilePath ' C:\MySources\XML\
```

<br />

# Group Property #

Gets or sets Notification group.
```VBS
Public Property Get Group() As String
Public Property Let Group(NewGroup As String)
```
## Property Value ##
[String](https://docs.microsoft.com/en-us/dotnet/visual-basic/language-reference/data-types/string-data-type "Stting reference.")

## Default Value ##
```VBS
"DefaultExcelNotificationGroup"
```

<br />

# IsFileExist Property #

Checks, if file [FileName](#filename-property) on [FilePath](#filepath-property) exists.
```VBS
Public Property Get IsFileExist() As Boolean
```
## Property Value ##
[Boolean](https://docs.microsoft.com/en-us/dotnet/visual-basic/language-reference/data-types/boolean-data-type "Boolean reference.")

<br />

# Sound Property #

Sets sound, which will be played when Notification is displayed.
```VBS
Public Property Let Sound(NewSound As DefaultWindowsSounds)
```
## Property Value ##
[Enum](https://docs.microsoft.com/en-us/office/vba/language/reference/user-interface-help/enum-statement "Enum reference.") [DefaultWindowsSounds](#defaultwindowssounds-enum "DefaultWindowsSounds Enum")

## Default Value ##
```VBS
DefaultWindowsSounds.Default
```

<br />

# Tag Property #

Gets or sets Notification's tag.
```VBS
Public Property Get Tag() As String
Public Property Let Tag(NewTag As String)
```
## Property Value ##
[String](https://docs.microsoft.com/en-us/dotnet/visual-basic/language-reference/data-types/string-data-type "String reference.")

## Default Value ##
```VBS
"DefaultExcelNotificationTag"
```

<br />

# TextAttribution Property #

Sets Notification's attribution text.
```VBS
Public Property Let TextAttribution(Text As String)
```
## Property Value ##
[String](https://docs.microsoft.com/en-us/dotnet/visual-basic/language-reference/data-types/string-data-type "String reference.")

<br />

# TextHighlighted Property #

Sets Notification's first line text (highlighted).
```VBS
Public Property Let TextHighlighted(Text As String)
```
## Property Value ##
[String](https://docs.microsoft.com/en-us/dotnet/visual-basic/language-reference/data-types/string-data-type "String reference.")

## Default Value ##
```VBS
"Excel information"
```

<br />

# TextLine1 Property #

Sets Notification's first non-highlighted text.
```VBS
Public Property Let TextLine1(Text As String)
```
## Property Value ##
[String](https://docs.microsoft.com/en-us/dotnet/visual-basic/language-reference/data-types/string-data-type "String reference.")

<br />

# TextLine2 Property #

Sets Notification's second non-highlighted text.
```VBS
Public Property Let TextLine2(Text As String)
```
## Property Value ##
[String](https://docs.microsoft.com/en-us/dotnet/visual-basic/language-reference/data-types/string-data-type "String reference.")

<br />

---

<br />

# CreateFile Method #
Create XML source file.

## Syntax ##
*expression*.**CreateFile** (*[Path]*, *[Name]*, *[Verbose]*)  
*expression* A variable that represenets [SourceXMLFile](#sourcexmlfile-class "SourceXMLFile class") object.

## Parameters ##
| Name | Required/Optional | Data type | Description |
|---|---|---|---|
| *Path* | Optional | String | Path, where file will be saved. **(Default:** [FilePath](#filepath-property "FilePath Property") **)** |
| *Name* | Optional | String | New name of the file. **(Default:** [FileName](#filename-property "FileName Property") **)**|
| *Verbose* | Optional |  Boolean | Inform user in [Immediate window](https://docs.microsoft.com/en-us/office/vba/language/reference/user-interface-help/immediate-window "Immediate window reference.") with details when the file will be create. **(Default:** False **)**|

<br />

## Remarks ##

<br />

# ProgressBar Method #

## Syntax ##
*expression*.**ProgressBar** (*Status*, *[Value]*, *[Title]*, *[ValueStringOverride]*)  
*expression* A variable that represenets [SourceXMLFile](#sourcexmlfile-class "SourceXMLFile Class") object.

## Parameters ##
| Name | Required/Optional | Data type | Description |
|---|---|---|---|
| *Status* | Required | String | Text appearing under the progress bar. |
| *Title* | Optional | String | Title of progress bar. **(Default:** blank **)**|
| *Value* | Optional | Double | Value of progress bar. **(Default:** 0.0 **)**|
| *ValueStringOverride* | Optional | String | Text which will replace `Value`. **(Default:** `Value`% (0%) **)**|


<br />

# Remarks #
More details on official MS documentation webpage ["Toast progress bar and data binding".](https://docs.microsoft.com/en-us/windows/apps/design/shell/tiles-and-notifications/toast-progress-bar?tabs=builder-syntax "Details of Toast notification's progress bar.")

<br />

# Send Method #

## Syntax ##
*expression*.**Send** (*[NotificationGroup]*, *[NotificationTag]*)  
*expression* A variable that represenets [SourceXMLFile](#sourcexmlfile-class "SourceXMLFile Class") object.

## Parameters ##
| Name | Required/Optional | Data type | Description |
|---|---|---|---|
| *NotificationGroup* | Optional | String | **(Default:** [Group](#group-property "Group Property") **)**|
| *NotificationTag* | Optional | String | **(Default:** [Tag](#tag-property "Tag Property") **)**|

<br />

# ***Support and Feedback*** #
`Created by: ©BaronProsimir 2021`  
`Contact:` [BaronProsimir@gmail.com](mailto:baronprosimir@gmail.com "Private email address.")  
`Licence:` [`GPL-3.0`](https://www.gnu.org/licenses/gpl-3.0.en.html)  
`Version:` `0.0.1`  

<br />